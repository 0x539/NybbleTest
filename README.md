# NybbleTest #

## The NybbleTest subgame for the Minetest engine ##


To use this subgame with the Minetest engine, insert this repository as
	/games/NybbleTest

The Minetest engine can be found in:
	https://github.com/minetest/minetest/

License of source code
----------------------
Copyright (C) 2010-2012 celeron55, Perttu Ahola 
<celeron55@gmail.com>

Copyright (C) 2016 0x539, Corentin Bocquillon 
<0x539+framagit@nybble.fr>

See README.txt in each mod directory for information about other 
authors.

This program is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the 
Free Software Foundation, either version 3 of the License, or (at your 
option) any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

You should have received a copy of the GNU General Public License along 
with This program. If not, see http://www.gnu.org/licenses/.

License of media (textures and sounds)
--------------------------------------
Copyright (C) 2010-2012 celeron55, Perttu Ahola 
<celeron55@gmail.com>
See README.txt in each mod directory for information about other 
authors.

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

License of menu/header.png
Copyright (C) 2016 0x539, Corentin Bocquillon 
<0x539+framagit@nybble.fr>
This work is licensed under a Creative Commons Attribution-ShareAlike 
4.0 International License.